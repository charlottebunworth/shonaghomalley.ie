Title: Calm, Confident and Consistent parenting
Date: 2019/02/11

**Calm, Confident and Consistent parenting workshop in the Cork Airport Hotel.**

### Date and Time
Sat, 16 February 2019
<br>10:30 – 14:30 GMT

### Location
Cork Airport Hotel
<br>Cork airport
<br>Cork

To book your spot on this workshop head over to Eventbrite:

[https://www.eventbrite.ie/e/calm-confident-and-consistent-parenting-tickets-56110132918?ref=eios](https://www.eventbrite.ie/e/calm-confident-and-consistent-parenting-tickets-56110132918?ref=eios)
