Title: Licences
Date: 2018/11

### shonaghomalley.ie licencing information


<br>All photographs are licenced <a href="https://creativecommons.org/licenses/by-nc-nd/4.0/">CC BY-NC-ND</a>
<br>Caterpillar illustration by <a href="clarebreen.net">Clare Breen</a> — <a href="https://creativecommons.org/licenses/by-nc/4.0/">CC BY-NC</a>

Website built with the static site generator [pelican](getpelican.com) based off the [Flex theme](https://github.com/alexandrevicenzi/Flex) by alexandrevicenzi [MIT licence](https://github.com/alexandrevicenzi/Flex/blob/master/LICENSE)

Website by <a href="colm.be">Colm O'Neill</a> licenced <a href="https://creativecommons.org/licenses/by/4.0">CC BY</a> and sources located at <a href="gitlab.com/colmoneill/shonaghomalley.ie/">gitlab.com/colmoneill/shonaghomalley.ie</a>
