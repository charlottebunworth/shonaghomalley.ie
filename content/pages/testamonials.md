Title: Testamonies
Date: 2018-11-15

## What people said about working with Shonagh

from [the facebook page](https://www.facebook.com/shonaghomalleyadvisor/):

> *I have started to use some of the tips and reward stickers that shonagh put a plan in place for my seven year old.already I can see a difference simple tasks dont take as long .we have bonded immensely with activities like swimming and baking.I totally recommend shonagh she was such a fantastic help to me and children.*
<br><br>— Nicola Stead - Mayo

&nbsp;

> *'#ThisWorkshopWorksPeople!!' Having had "more" than a couple of days since participating in this workshop by Shonagh O Malley (I like to absorb and put into action what i have learnt before penning any review, be it a restaurant, a holiday, or even bottle of wine). I found myself being the only male in this workshop and being a self confessed "knowitall" of my 11 year old daughter (going on 18) entered in to Shonagh's workshop!
<br>This woman is good...having straight away acknowledged my apprehensions, she quickly set mine and the rest of the classes minds at ease with her calm yet affirmative tone. I quickly became engaged in all that matters with behavioral childcare and how to deal with such.
<br>Our class was both refreshing and engaging, all involved learning not only from Shonagh but each other and great debates followed.
Having had near on no professional guidance on knowing how to manage mine or any childs behavioral skills, our workshop involved some role playing scenarios and home hitting truths that did nothing but improve my interaction and time quality with my daughter....for that alone, I am grateful and so looking forward to when Shonagh is in my area again.
<br><br>EDIT, I've since learnt that Shonagh is available online, via video call to answer all and any of my follow on questions.
<br>FABULOUS  WORKSHOP, FABULOUS WOMAN, this girl knows her stuff...........looking forward to attending another workshop soon (my daughter is getting closer to 12 now so I've sooooo many more questions, and Shonagh is now my “go-to pro”
<br><br>From a most grateful dad, thank you Shonagh*
<br><br>— Trevor Noonan - Cork

&nbsp;

> *“I’m just home from a parenting workshop with Shonagh and I am bursting with positivity. It was certainly time very well spent and I am looking forward to putting into action the tips and tricks that I learned today. Shonagh has a great way of reassuring you that anything is achievable in parenting once you are willing to put in the work and be consistent. I cannot recommend Shonagh enough. As a mother of three, ranging in age from 2 to 11, I know it’s never too late to seek advice and help. No parent is perfect but we can all strive to be <a>good enough</a> 💪”*
<br><br>— Niamh - Cork

&nbsp;

> *“In one week I could see a much improved behaviour in my son. Shonagh put a plan in place for me and my family. She showed such compassion and understanding. To keep going and follow through has made such a difference. Shonagh is always at the end of phone if I need her as she said she is here to help. I am so thankful to Shonagh for helping me and my family. The results are amazing. Thank you Shonagh.”*
<br><br>— Nicola Stead and family
