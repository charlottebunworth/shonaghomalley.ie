Title: About Shonagh
Date: 2018-11-15

### About Shonagh O'Malley

![](../images/shonagh.jpg){.medium-left}

Having worked in childcare for nearly twenty years, I have always been amazed that parents are given lots of advice on bathing babies and feeding babies while they are expecting, but as soon as you are sent home from the hospital you are on your own. We are told *“mother knows best”* and *“it’s the terrible two’s”* or *“it’s a phase, they will grow out of”*. But in this busy high pressure world we live in, parents are facing more challenges than ever before.

Stress, guilt, exhaustion as well as lack of understanding of why our children are behaving in certain ways lead us into problems. Many of the parents I have met down through the years have fallen into bad habits with their children and now want to parent their children in a positive way in order to enjoy a happy and calm family life. My mission is to help parents learn the skills they need to parent in an effective and positive way that will help children grow in confidence and become well rounded adults.

![](../images/marathon.jpg){.medium-left}

[This article in the Echo](http://www.echolive.ie/wow/Im-going-to-do-the-best-from-here-on-in-after-my-cancer-battle-63e83b59-33af-46e1-9d44-b5488905ac77-ds) might also tell you a bit more about the kind of person I am.
