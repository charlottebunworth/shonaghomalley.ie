Title: Outdoor water play
Date: 2018-11-16
Cover: ../images/water-detail.jpeg

### Extended outdoor water play

![](../images/image2.jpeg)

Extended learning and creative play can keep children busy in the sunshine for hours.
