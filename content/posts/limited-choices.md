Title: Limited choices
Date: 2018/11/28

Limited choices and the benefits of letting your children make their own decisions

<video width="40%" height="auto" controls>
 <source src="../images/video.mp4" type="video/mp4">
 Your browser does not support the video tag.
</video>
