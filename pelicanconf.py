#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = "Shonagh O'Malley"
SITENAME = "Shonagh O'Malley — Parenting Advisor"
SITEURL = "shonaghomalley.com"

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Dublin'

DEFAULT_LANG = u'en'

ROBOTS = 'index, follow'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME_STATIC_DIR = 'theme/flex'
THEME = 'theme/flex/'

MENUITEMS = (('About Shonagh', '/pages/about-shonagh.html'),
             ('Testamonies', '/pages/testamonies.html'),
             ('Posts', '/category/posts.html'),
             #('Pricings', '/pages/pricings.html'),
             ('Contact', '/pages/contact.html'),)

# Blogroll
LINKS = (("", ''),
         ('', ''),
         ('', ''),
         ('', ''),)

CC_LICENSE = {
    'name': 'Creative Commons Attribution-ShareAlike',
    'version': '4.0',
    'slug': 'by-sa'
}

COPYRIGHT_YEAR = 2019

COPYRIGHT_NAME = "Shonagh O'Malley"

# Social widget
SOCIAL = (('facebook', 'https://www.facebook.com/shonaghomalleyadvisor/'),
          ('instagram', '#'),
          ('twitter', '#'),
          ('rss', '/feeds/all.atom.xml'),)

DEFAULT_PAGINATION = 5

DISPLAY_PAGES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
